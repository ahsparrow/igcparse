from setuptools import setup, find_packages

setup(
    name="igcparse",
    packages=["igcparse"],
    version="0.3",
    description="IGC file parser",
    url="https://gitlab.com/ahsparrow/igcparse.git",
    author="Alan Sparrow",
    author_email="igcparse@freeflight.org.uk",
    license="GPL",
    install_requires=["numpy>=1.13"],
)
