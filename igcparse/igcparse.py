import codecs
import datetime
import re
import time

import numpy as np

A_REC_RE = "A(\w{3})(\w{3})"
H_DATE_REC_RE = "HFDTE[A-Za-z]?:?(\d{6})"
I_REC_RE = "I(\d{2})(.*)"
B_REC_RE = "B(\d{2})(\d{2})(\d{2})(\d{2})(\d{5})([NS])(\d{3})(\d{5})([EW])A(\d{5})(\d{5})"

IGC_TLA = {
    'siu': 'i8',
    'fxa': 'i8'
}

def igcparse(igc_file):
    """Read data from an IGC file

    Args:
        igc_file: IGC file object

    Returns:
        array of fixes
    """

    # Read A record
    rec = igc_file.readline()
    if not rec.startswith("A"):
        print("Missing A record")

    # Read header
    header = {}
    rec = igc_file.readline()
    if not rec.startswith("H"):
        print("Missing H record")

    while rec.startswith("H"):
        key = rec[2:5].lower()
        if ":" in rec:
            header[key] = rec.split(":")[1].strip()
        else:
            header[key] = rec[5:-1]
        rec = igc_file.readline()

    # Find I record
    while not rec.startswith('I'):
        rec = igc_file.readline()

    # Get number of additions
    m = re.match(I_REC_RE, rec)
    n_add = int(m.group(1))

    # Modified I record RE match additions
    irec_re = "I\d{2}" + "(\d{2})(\d{2})([A-Z]{3})" * n_add
    m = re.match(irec_re, rec)

    # Base RE and in/out dtypes
    brec_re = B_REC_RE
    in_types = [('hour', 'i8'), ('min', 'i8'), ('sec', 'i8'),
                ('lat_deg', 'i8'), ('lat_min', 'i8'), ('ns', "S1"),
                ('lon_deg', 'i8'), ('lon_min', 'i8'), ('ew', "S1"),
                ('alt', 'i8'), ('alt_gps', 'i8')]
    out_types = [('utc', 'i8'),
                 ('lat', 'f8'), ('lon', 'f8'),
                 ('alt', 'i8'), ('alt_gps', 'i8')]

    # Additions
    add_types = []
    for n in range(n_add):
        add_id = m.group(n * 3 + 3).lower()
        add_len = int(m.group(n * 3 + 2)) - int(m.group(n * 3 + 1)) + 1
        add_type = IGC_TLA.get(add_id, "a%d" % add_len)

        brec_re += "([A-Z0-9\-]{%d})" % add_len
        add_types.append((add_id, add_type))

    # Parse B records
    igc = np.fromregex(igc_file, brec_re, in_types + add_types)

    data = np.zeros(igc.shape[0], dtype=out_types + add_types)

    # UTC times
    data['utc'] = igc['hour'] * 3600 + igc['min'] * 60 + igc['sec']

    # Latitude/longitude
    lat = np.radians(igc['lat_deg'] + igc['lat_min'] / 60000)
    lon = np.radians(igc['lon_deg'] + igc['lon_min'] / 60000)

    ones = np.ones(len(lon))
    data['lat'] = lat * np.where(igc['ns'] == b"N", ones, -ones)
    data['lon'] = lon * np.where(igc['ew'] == b"E", ones, -ones)

    data['alt'] = igc['alt']
    data['alt_gps'] = igc['alt_gps']

    for id, dtype in add_types:
        data[id] = igc[id]

    return {'header': header, 'data': data}

if __name__ == "__main__":
    import sys
    import matplotlib.pyplot as plt

    igc = igcparse(open(sys.argv[1], "r", errors='ignore'))
    print(igc['header'])
    #plt.plot(igc['alt'])
    #plt.show()
